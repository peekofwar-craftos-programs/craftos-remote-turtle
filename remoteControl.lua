local screen = {}
screen.x,screen.y=term.getSize()

pos = {
    get = function()
        return term.getCursorPos()
    end,
    set = function(x,y)
        term.setCursorPos(x,y)
    end,
}
function printTable(x)
    for k, v in pairs(x) do
        if type(v) == 'table' then
            printTable(v)
        else
            print(k,v)
        end
    end
end
function run()
    term.clear()
    pos.set(1,1)
    write('Enter turtle ID> ')
    turtleID = tonumber(io.read())
    
    while true do
        write('Input> ')
        userString = io.read()
        rednet.send(turtleID, userString)
        id,msg,protocol = rednet.receive(5)
        if msg then
            if protocal == 'ERROR' then
                printError('Error: '..tostring(msg))
            else
                if type(msg) == 'table' then
                    printTable(msg)
                else
                    print('Response: '..tostring(msg))
                end
            end
        else
            printError('No response from '..turtleID)
        end
    end
end
ok, err = pcall(run)
if err then printError(err)  sleep(5) end
