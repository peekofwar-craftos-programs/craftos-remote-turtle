config = {
    id_filter = {},
--[[Seperate IDs by camma, if none
    are specified, then it will accept
    any, ]]
    modem_id = 'right',
}

message = {}
function listen()
    write('Awaiting input... ')
    message.id,message.string,message.protocol = rednet.receive()
    print('Received: '..message.string)
end
function verifyPermission()
    if #config.id_filter == 0 then
        return true
    else
        for i=1, #config.id_filter do
            if message.id == config.id_filter[i] then
                return true
            end
        end
        return false
    end
end
function splitString(str)
    local t = {}
    for i in string.gmatch(str"([^ ]+)") do
        table.insert(t, i)
    end
    return t
end
function runString(x)
    local c = assert(load('return '..x))
    return c()
end
function run()
    while true do
        listen()
        if verifyPermission() then
            print('ID: '..message.id..', cmd: '..message.string)
            results = {pcall(runString, message.string)}
            ok = results[1] err=results[2]
            result = results table.remove(result, 1)
            if not ok then
                printError(err)
                rednet.send(message.id, err, 'ERROR')
            else
                rednet.send(message.id, result)
            end
        else
            printError('Rejected input from '..message.id)
            rednet.send(message.id, 'NOT AUTHORIZED TO SEND COMMANDS!')
        end
        result = nil results = nil
    end
end

if not rednet.isOpen(config.modem_id) then
    print('Opening modem...')
    rednet.open(config.modem_id)
end
run()
